import subprocess

login = None
password = None

it_workers = ['pbogus']
ksiegowosc_workers = ['ksiegowa']

def welcome_screen():
    print(''' _______     _
(_) ___ \   (_)
 _| |_/ / __ _ _ __ ___   ___
| |  __/ '__| | '_ ` _ \ / _ \\
| | |  | |  | | | | | | |  __/
|_\_|  |_|  |_|_| |_| |_|\___|''', 'Drive Mapper v. 0.1 (25.08.2020)\n')

def choice_menu_option():
    menu_choice = input("MENU GŁÓWNE\n"
          "--------------------------------------------------\n"
          "Wybierz opcję:\n"
          "--------------------------------------------------\n"
          "1. Zaloguj się i zamontuj odpowiednie dyski sieciowe\n"
          "2. Sprawdź aktywne połączenia z dyskami sieciowymi\n"
          "3. Odmontuj dyski sieciowe\n"
          "(Wprowadź odpowiedni numer i zatwierdź za pomocą klawisza ENTER.)\n"
          "4. Wyjdź z programu\n")

    return menu_choice

def writing_login_and_password():
    login = input("Podaj nazwę użytkownika: ")
    password = input("Podaj hasło: ")
    return login, password

def making_access(drive_letter, folder_name):
    subprocess.call(f'net use {drive_letter}: \\\\192.168.0.39\\{folder_name} /user:{login} {password}', shell=True,
                    stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

if __name__ == '__main__':
    while True:
        welcome_screen()
        menu_choice = choice_menu_option()

        if menu_choice == '1':
            login, password = writing_login_and_password()

            if login in it_workers:
                making_access("P", "publiczny")
                making_access("I", "dzial_it")

            elif login in ksiegowosc_workers:
                making_access("P", "publiczny")
                making_access("K", "ksiegowosc")

            else:
                print("Użytkownik nie istnieje!")

            input('Wciśnij ENTER aby kontynuować.')

        elif menu_choice == '2':
            subprocess.call('net use', shell=True)
            input('Wciśnij ENTER aby kontynuować.')

        elif menu_choice == '3':
            list_of_disks_to_delete = ['I', 'P', 'K']
            for disks in list_of_disks_to_delete:
                subprocess.call(f'net use {disks}: /Delete', shell=True, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
            input("Pomyślnie odmontowano dyski lokalne.\n(Wciśnij ENTER aby kontynuować.)\n")

        elif menu_choice == '4':
            exit()

        else:
            input("Dokonałes niewłaściwego wyboru! Naciśnij ENTER by wrócić do MENU.")